<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['namespace' => 'Auth'], function () {
    Route::post('/register', 'RegisterController@signUp')->middleware('guest');
    Route::get('otp', 'RegisterController@otp')->name('otp');
    Route::post('/confirm/code', 'RegisterController@verifyOtp')->name('confirm.code');
    Route::post('/resend/otp', 'RegisterController@resendOtp')->name('resend.opt');
    Route::post('login', 'LoginController@login')->name('login');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('create-task', 'TaskController@create')->name('user.create.task');
Route::post('store-task', 'TaskController@store')->name('user.store.task');
Route::get('listing-task', 'TaskController@index')->name('user.listing.task');
Route::get('edit-task', 'TaskController@edit')->name('user.edit.task');
Route::get('delete-task', 'TaskController@destroy')->name('user.delete.task');



