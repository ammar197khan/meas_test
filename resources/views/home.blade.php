@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Dashboard</h3>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <p>{{ __('You are logged in!') }}</p>
    </div>
@endsection

