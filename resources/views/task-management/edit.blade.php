@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h1>Edit ToDo</h1>
    </div>
    <div class="container">
        @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
            @include('error-messages')
        @endif
        @if (count($errors) > 0)
            @include('error-messages')
        @endif
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6"><form action="{{ route('user.store.task') }}" method="post">
                    @csrf
                    <input type="hidden" value="{{ $task->id }}" name="id">
                    <div class="form-group">
                        <label for="usr">Task</label>
                        <input type="text" class="form-control" value="{{ $task->title }}" name="title" id="usr">
                    </div>
                    <div class="form-group">
                        <label for="comment">Description</label>
                        <textarea class="form-control" rows="5" id="comment" name="description" value="{{ $task->description }}">{{ $task->description }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form></div>

            <div class="col-lg-3"></div>
        </div>

    </div>
@endsection
