@extends('layouts.app')
@section('content')
    <div class="jumbotron">
        <h1>Todo List</h1>
    </div>
    <div class="container mb-5">
        @if(!empty( \Illuminate\Support\Facades\Session::get('message') ))
            @include('error-messages')
        @endif
        @if (count($errors) > 0)
            @include('error-messages')
        @endif
            <a href="{{ route('user.create.task') }}" class="btn btn-primary" style="    position: absolute;
       right: 121px;
    top: 212px;">Create</a>

            {!! $dataTable->table() !!}


    </div>
@endsection
@push('scripts')
    {!! $dataTable->scripts()  !!}
@endpush

