<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Repositories\TaskRepo;
use Illuminate\Support\Facades\Config;

class TaskServices
{
    /**
     * @var
     */
    protected $taskRepo;

    /**
     * TaskServices constructor.
     * @param TaskRepo $taskRepo
     */
    public function __construct(){
        $taskRepo = new TaskRepo();
        $this->taskRepo  =  $taskRepo;

    }

    /**
     * @param $request
     * @return string
     */
    public function store($request){
        $data =  $this->taskRepo->store($request);
        return $data;
    }

    /**
     * @param $request
     * @return string
     */
    public function destroy($request){
        $data =  $this->taskRepo->destroy($request);
        return $data;
    }
    public function edit($request){
        $data =  $this->taskRepo->edit($request);
        return $data;
    }






}
