<?php

namespace App\Http\Controllers;
use App\Http\Requests\ToDoRequest;
use App\Services\TaskServices;
use App\DataTables\TasksDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TaskController extends Controller
{
    protected $taskService;
    public function __construct(){
        $taskService   =  new TaskServices();
        $this->taskService = $taskService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(TasksDataTable $tasksDataTable)
    {
        //
        return $tasksDataTable->render('task-management.listing');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('task-management.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ToDoRequest $request)
    {
        //

        $request->request->add(['user_id' => Auth::user()->id]);
        $this->taskService->store($request);
        if (!empty($request->id)){
            session::flash('alert-class',"alert-success");
            session::flash('message','Task created successfully!');
            return redirect()->route('user.listing.task');
        }else{
            session::flash('alert-class',"alert-success");
            session::flash('message','Task updated successfully!');
            return redirect()->route('user.listing.task');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
          $task = $this->taskService->edit($request);
          return view('task-management.edit')->with(['task' => $task ]) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        $this->taskService->destroy($request);
        session::flash('alert-class',"alert-success");
        session::flash('message','Task deleted successfully!');
        return back();
    }
}
