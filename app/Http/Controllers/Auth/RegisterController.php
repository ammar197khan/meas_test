<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Models\Otp;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $otp ='';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $otp        = new Otp;
        $this->otp  = $otp;

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

    }

    public function signUp(RegisterRequest $request){

        $user_type = 'App\\Models\\User';
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        $this->otp->where('relational_type', 'App\\Models\\User')->where('email', $request->email)->delete();
        $this->otp->saveOrUpdate($this->otp, [
            'otp' => generateOTP(),
            'email' => $user->email,
            'name' => $user->name,
            'number' => $user->contact_no,
            'user_id' => $user->id,
            'user_type' => $user_type,
        ], true);
        Session::put('userOtpEmail', $request->email);
        session::flash('alert-class',"alert-success");
        session::flash('message', 'We send you an OTP, Please verify your email address!');
        return redirect()->route('otp');


    }

    public function otp(Request $request){
        return  view('auth.otp');
    }

    /**
     * verifyOtp function
     *
     * @param RegisterOtpRequest $request
     * @return void
     */
    public function verifyOtp(Request $request)
    {


        $otp = $this->otp->where('otp_code', $request->input('verification_code'));

        if(!empty($request->email)){
            $otp = $otp->where('email',$request->input('email'));
        }
        $otp = $otp->latest()->first();
        if (!empty($otp) && !$otp->isExpire()) {
                $user = User::where('email',$otp->email)->first();


                if (!empty($user)) {
                    if (!empty($request->input('password'))) {
                        $user->setAttributeValue('password', Hash::make($request->input('password')))->save();
                    }
                    $user->email_verified_at = Carbon::now();
                    $user->save();
                    $otp->delete();
                    session::flash('alert-class',"alert-success");
                    session::flash('message', 'OTP verified successfully!');
                    return redirect()->route('login');

                }
        }
        session::flash('alert-class',"alert-success");
        session::flash('message', 'OTP not exist!');
        return back();
    }

    /**
     * resendOtp function
     *
     * @param Request $request
     * @return void
     */
    public function resendOtp(Request $request)
    {

        $user = User::where('email', $request->email)->first();
        $response = 'Email not belong to!';
        if(!Auth::check() || (Auth::check() && Auth::user()->email == $request->email)){
            $response ='Invalid Email not belong to!';
            if (!empty($user)) {
                $this->otp->where('relational_type', 'App\\Models\\User')->where('email', $request->email)->delete();
                $this->otp->saveOrUpdate($this->otp, [
                    'otp' => generateOTP(),
                    'email' => $user->email,
                    'name' => $user->name,
                    'number' => $user->contact_no,
                    'user_id' => $user->id,
                    'user_type' => 'App\\Models\\User',
                ], true);
                $response = 'An OPT Send to email address!';
            }
        }
        session::flash('alert-class',"alert-success");
        session::flash('message',$response);
        return back();
    }

}
