<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $user = null;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $user = new User();
        $this->user  = $user;
    }

    public function login(LoginRequest $request)
    {


        $object = $this->user;
        if (!Auth::attempt(request(['email', 'password']))) {
            session::flash('alert-class',"alert-success");
            session::flash('message','These credentials do not match our records!');
            return back();
        }
        $object = $object->where('email', $request->email)->first();
        if (empty($object) || !Hash::check($request->password, $object->password, [])) {
            Auth::logout();
            session::flash('alert-class',"alert-success");
            session::flash('message','Invalid credentials!');
            return back();

        }
        if (empty($object->email_verified_at)) {
            Auth::logout();
            if (!empty($request->otp_verify)) {
                //On request of arman change response
                session::flash('alert-class',"alert-success");
                session::flash('message','Email not verified yet!');
                return back();

            }
            session::flash('alert-class',"alert-success");
            session::flash('message','Email not verified yet!');
            return back();
        }

       return redirect()->route('home');
    }
}
