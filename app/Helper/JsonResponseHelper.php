<?php

use Illuminate\Support\Facades\Config;



/**
 * @param $data
 * @param string $message
 * @param int $code
 * @return \Illuminate\Http\JsonResponse
 */
function statusResponseSuccess($data, $message = '', $code = 200)
{
    return response()->json(
        [
            'code' => $code,
            'data' => $data,
            'status' => true,
            'message' => $message,
        ],
        $code
    );
}

