<?php
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * @return string
 */
function generateOTP()
{
    return (string)rand(100000, 999999);
}
