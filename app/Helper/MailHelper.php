<?php

use Carbon\Carbon;

use App\Mail\SignupEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/**
 * @param $name
 * @param $email
 * @param $otp
 */
function sendOtp($name, $email, $otp)
{
    $data = [
        'name' => $name,
        'otp' => $otp,
        'time' => config('app.otp_expiry'),
        'email' => $email
    ];
    Mail::send('mail.otp', $data, function ($message) use ($data) {
        $message->to($data['email'])->subject('mail send');
    });
}

