<?php

namespace App\DataTables;

use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class TasksDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('edit', function ($query) {
                return '<a href="' . route('user.edit.task',['id' => $query->id ]  ) . '" class="btn btn-primary"> Edit</a>';
            })
            ->addColumn('delete', function ($query) {
                return '<form action="' . route('user.delete.task' ) . '" onsubmit="return confirm(\'Are you sure to delete?\')" method="get"> <input type="hidden" name="_method" value="DELETE"> <input type="hidden" name="_token" value="' . csrf_token() . '"> <input type="hidden" name="id" value="' .  $query->id . '">  <button class="btn btn-danger">Delete</button> </form>';
            })
            ->rawColumns(['delete', 'edit']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Task $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Task $model)
    {
        $userId= Auth::user()->id;
        return $model->where('user_id', $userId)->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('tasks-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('Sr#')
                     ->name('id')
                     ->data('id'),
            Column::make('title')
                ->name('title')
                ->data('title'),
            Column::make('description')
                ->name('description')
                ->data('description'),
            Column::make('edit')
                ->sortable(false)
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
            Column::make('delete')
                ->sortable(false)
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Tasks_' . date('YmdHis');
    }
}
