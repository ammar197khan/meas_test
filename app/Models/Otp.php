<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    //


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'otp',
        'email'
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'expire_at' => 'datetime',
    ];
    /**
     * ---------------------------Mutators---------------------------------------
     */


    public function setOtpCode($value)
    {
        $this->attributes['otp_code'] = $value;
        return $this;
    }
    public function setEmail($value)
    {
        $this->attributes['email'] = $value;
        return $this;
    }
    public function setRelationalId($value)
    {
        $this->attributes['relational_id'] = $value;
        return $this;
    }
    public function setRelationalType($value)
    {
        $this->attributes['relational_type'] = $value;
        return $this;
    }
    public function setExpiry()
    {
        $this->attributes['expire_at'] = Carbon::now()->addMinutes(30);
        return $this;
    }
    public function scopeIsExpire()
    {
        return !Carbon::parse($this->attributes['expire_at'])->isFuture();
    }

    public function scopeSaveOrUpdate($query,$object,$data,$sendOtp = false){
        $object->setOtpCode($data['otp'])
            ->setEmail($data['email'])
            ->setRelationalId($data['user_id'])
            ->setRelationalType($data['user_type'])
            ->setExpiry()
            ->save();
        if($sendOtp){
            sendOtp(isset($data['name']) ? $data['name'] : '', $data['email'], $data['otp']);
            // $this->sendSms($data['number'],'Verification Code: '. $data['otp']);
        }
        return $object;
    }

}
