<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Task extends Model
{
    //
    protected $table = 'tasks';
    protected $fillable = ['title', 'description', 'user_id'];
    public $timestamps = false;

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function setAttributeValue($name,$value)
    {
        if(!empty($value) && $value !== 'undefined'){
            $this->attributes[$name] = $value;
        }
        return $this;
    }

    /**
     * @param $query
     * @param $user
     * @param $request
     * @return mixed
     */
    public function scopeSaveOrUpdate($query,$objectTask,$request){

        $objectTask
            ->setAttributeValue('title',$request->input('title'))
            ->setAttributeValue('description',$request->input('description'))
            ->setAttributeValue('user_id',$request->input('user_id'))
            ->save();
        return $objectTask;
    }
    public function scopeTaskFirst($query, $request){
        $userId = Auth::user()->id;
      return  $query->where('id', $request->id)->where('user_id', $userId)->first();
    }
}
