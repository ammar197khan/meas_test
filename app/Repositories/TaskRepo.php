<?php

namespace App\Repositories;


use App\User;
use App\Models\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;


class TaskRepo
{
    /**
     * @var User
     */
    private $userModel;
    /**
     * @var Task
     */
    private $taskModel;


    /**
     * TaskRepo constructor.
     */
    public function __construct()
    {
        $userModel =  new User();
        $this->userModel = $userModel;
        $taskModel  =   new Task();
        $this->taskModel = $taskModel;

    }



    /**
     * @param Request $request
     */
    public function store(Request $request){

        if (!empty($request->id)){

            $taskModel = $this->taskModel->taskFirst($request);
            if (!empty($taskModel)){
                $taskModel = $this->taskModel->saveOrUpdate($taskModel, $request);
            }
        }else{
            $taskModel = $this->taskModel->saveOrUpdate($this->taskModel, $request);
        }

       return $taskModel;
    }

    /**
     * @param Request $request
     * @return string
     */
    public function destroy(Request $request){
        $userId = Auth::user()->id;
       $taskModel = $this->taskModel->where('id', $request->id)->where('user_id', $userId)->first();
       if (!empty($taskModel)){
           $this->taskModel->where('id', $request->id)->delete();
       }
       return 'success';
    }
    public function edit(Request $request){

        $taskModel = $this->taskModel->taskFirst($request);
        return $taskModel;
    }

}




